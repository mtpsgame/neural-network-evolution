﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Genetic {

    private int populationSize;
    private int[] networkTopology;

    private int tournamentSize;
    private float mutationRate;
    private float minNetworkWeight;
    private float maxNetworkWeight;

    public Genetic(int populationSize, int[] networkTopology, int tournamentSize, float mutationRate, float minNetworkWeight, float maxNetworkWeight) {
        this.populationSize = populationSize;
        this.networkTopology = networkTopology;
        this.tournamentSize = tournamentSize;
        this.mutationRate = mutationRate;
        this.minNetworkWeight = minNetworkWeight;
        this.maxNetworkWeight = maxNetworkWeight;
    }

    public NeuralNetwork[] GenerateInitialPopulation() {
        NeuralNetwork[] initialNetworks = new NeuralNetwork[populationSize];
        for (int i = 0; i < populationSize; i++) {
            initialNetworks[i] = new NeuralNetwork(networkTopology, minNetworkWeight, maxNetworkWeight);
            initialNetworks[i].RandomiseWeights();
        }
        return initialNetworks;
    }

    public NeuralNetwork[] GenerateNewPopulation(List<Entity> oldPopulation) {
        NeuralNetwork[] children = new NeuralNetwork[populationSize];
        
        for (int i = 0; i < populationSize; i++) {
            Entity parentOne = TournamentSelection(oldPopulation);
            Entity parentTwo = TournamentSelection(oldPopulation);

            children[i] = CrossoverParentsAndMutate(parentOne.GetNetwork(), parentTwo.GetNetwork());
        }

        return children;
    }

    private Entity TournamentSelection(List<Entity> entities) {
        return entities.OrderBy(e => Random.Range(0f, 1f)).Take(tournamentSize).OrderByDescending(e => e.GetFitness()).ElementAt(0);
    }

    private NeuralNetwork CrossoverParentsAndMutate(NeuralNetwork parentOne, NeuralNetwork parentTwo) {
        NeuralNetwork[] parents = { parentOne, parentTwo };
        NeuralNetwork child = new NeuralNetwork(networkTopology, minNetworkWeight, maxNetworkWeight);

        for (int layer = 0; layer < networkTopology.Length - 1; layer++) {

            for (int inputNode = 0; inputNode < child.GetLayers()[layer].GetInputCount(); inputNode++) {

                for (int outputNode = 0; outputNode < child.GetLayers()[layer].GetOutputCount(); outputNode++) {

                    if (Random.Range(0f, 1f) < mutationRate) {

                        child.GetLayers()[layer].GetWeights()[inputNode, outputNode] = Random.Range(minNetworkWeight, maxNetworkWeight);

                    } else {

                        int parentSelector = Random.Range(0f, 1f) <= 0.5f ? 0 : 1;
                        child.GetLayers()[layer].GetWeights()[inputNode, outputNode] = parents[parentSelector].GetLayers()[layer].GetWeights()[inputNode, outputNode];
                    }
                }
            }
        }
        return child;
    }
}
