﻿using System;
using UnityEngine;

public class NeuralLayer {

    private int inputCount, outputCount;
    private double[,] weights;

    public static double ActivationFunction(double x) {
        return Math.Tanh(x);
    }
    
    public NeuralLayer(int inputCount, int outputCount) {
        // Last input node is a bias.
        this.inputCount = inputCount + 1;
        this.outputCount = outputCount;
        weights = new double[this.inputCount, this.outputCount];
    }

    public void SetWeights(double[,] newWeights) {
        if (newWeights.GetLength(0) != inputCount || newWeights.GetLength(1) != outputCount) {
            throw new ArgumentException(String.Format("Wrong number of weights being set. Expected {0}x{1}, recieved {2}x{3}", weights.GetLength(0), weights.GetLength(1), newWeights.GetLength(0), newWeights.GetLength(1)));
        }

        weights = newWeights;
    }

    public void RandomiseWeights(float minValue, float maxValue) {
        for (int i = 0; i < inputCount; i++) {
            for (int j = 0; j < outputCount; j++) {
                weights[i, j] = UnityEngine.Random.Range(minValue, maxValue);
            }
        }
    }

    public double[] GetOutput(double[] input) {
        // Bias node not included in input, so have to minus one off inputCount.
        if (input.Length != inputCount - 1) {
            throw new ArgumentException(String.Format("Wrong number of inputs to a neural layer. Expected {0}, recieved {1}", inputCount, input.Length));
        }

        double[] outputs = new double[outputCount];

        for (int j = 0; j < outputCount; j++) {
            for (int i = 0; i < inputCount - 1; i++) {
                outputs[j] += weights[i, j] * input[i];
            }
            // Bias.
            outputs[j] += weights[inputCount - 1, j];
            outputs[j] = ActivationFunction(outputs[j]);
        }

        return outputs;
    }

    public int GetInputCount() {
        return inputCount;
    }

    public int GetOutputCount() {
        return outputCount;
    }

    public double[,] GetWeights() {
        return weights;
    }
}
