﻿using System;
using System.Collections;
using System.Collections.Generic;
public class NeuralNetwork {

    private int[] topology;

    private float minWeight;
    private float maxWeight;

    private NeuralLayer[] layers;

    public NeuralNetwork(int[] topology, float minWeight, float maxWeight) {
        this.topology = topology;
        this.minWeight = minWeight;
        this.maxWeight = maxWeight;
        layers = new NeuralLayer[topology.Length - 1];

        for (int i = 0; i < layers.Length; i++) {
            layers[i] = new NeuralLayer(topology[i], topology[i + 1]);
        }
    }

    public void RandomiseWeights() {
        for (int i = 0; i < layers.Length; i++) {
            layers[i].RandomiseWeights(minWeight, maxWeight);
        }
    }

    public void SetLayers(NeuralLayer[] newLayers) {
        if (newLayers.Length != topology.Length - 1) {
            throw new ArgumentException(String.Format("The neural network layers do not match the topology. Expected {0} layers, recieved {1}", topology.Length - 1, newLayers.Length));
        }

        for (int i = 0; i < newLayers.Length; i++) {
            if (newLayers[i].GetInputCount() != topology[i]) {
                throw new ArgumentException(String.Format("A neural network layer does not match the topology. Expected {0} input nodes, recieved {1}", topology[i], newLayers[i].GetInputCount()));
            }
            if (newLayers[i].GetOutputCount() != topology[i + 1]) {
                throw new ArgumentException(String.Format("A neural network layer does not match the topology. Expected {0} output nodes, recieved {1}", topology[i + 1], newLayers[i].GetOutputCount()));
            }
        }

        layers = newLayers;
    }

    public double[] GetOutput(double[] input) {
        double[] currentValues = input;

        for (int i = 0; i < layers.Length; i++) {
            currentValues = layers[i].GetOutput(currentValues);
        }

        return currentValues;
    }

    public NeuralLayer[] GetLayers() {
        return layers;
    }
}
