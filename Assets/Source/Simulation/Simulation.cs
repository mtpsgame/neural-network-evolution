﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Simulation : MonoBehaviour {

    [SerializeField]
    private int populationCount;

    [SerializeField]
    private int[] hiddenLayerTopology;

    [SerializeField]
    private int tournamentSize;

    [SerializeField]
    private float mutationRate;

    [SerializeField]
    private float minNetworkWeight;

    [SerializeField]
    private float maxNetworkWeight;

    [SerializeField]
    private GameObject entityPrefab;

    [SerializeField]
    private MovingWall movingWall;

    private int generation;
    private List<Entity> entities;

    private int[] networkTopology;

    private Genetic geneticAlgorithm;

    public void Start() {
        CreateTopology();
        geneticAlgorithm = new Genetic(populationCount, networkTopology, tournamentSize, mutationRate, minNetworkWeight, maxNetworkWeight);
        entities = new List<Entity>();
        GenerateInitialPopulation();
    }

    private void CreateTopology() {
        networkTopology = new int[hiddenLayerTopology.Length + 2];
        networkTopology[0] = 5;
        for (int i = 0; i < hiddenLayerTopology.Length; i++) {
            networkTopology[i + 1] = hiddenLayerTopology[i];
        }
        networkTopology[networkTopology.Length - 1] = 2;
    }

    private void GenerateInitialPopulation() {
        generation = 1;
        InstantiateEntities(geneticAlgorithm.GenerateInitialPopulation());
    }

    private void GenerateNextPopulation() {
        generation += 1;
        NeuralNetwork[] nextGeneration = geneticAlgorithm.GenerateNewPopulation(entities);

        foreach (Entity e in entities) {
            Destroy(e.gameObject);
        }

        entities.Clear();

        InstantiateEntities(nextGeneration);
    }

    private void InstantiateEntities(NeuralNetwork[] networks) {
        Debug.Log("Instantiating Entities for generation " + generation);

        movingWall.ResetWall();
        
        for (int i = 0; i < populationCount; i++) {
            Entity individual = Instantiate(entityPrefab).GetComponent<Entity>();
            individual.SetNetwork(networks[i]);
            entities.Add(individual);
        }
    }

    public void FixedUpdate() {
        if (entities.TrueForAll(e => e.HasCrahsed())) {
            Debug.Log("All have crashed! Moving on to the next generation.");
            GenerateNextPopulation();
        }

        Camera.main.transform.position = entities.Where(e => !e.HasCrahsed()).OrderByDescending(e => e.GetFitness()).ElementAt(0).transform.position - new Vector3(0, 0, 20);
    }
}
