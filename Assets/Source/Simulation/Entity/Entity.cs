﻿using UnityEngine;

public class Entity : MonoBehaviour {

    private const float movementSpeedMultiplier = 5f;
    private const float rotationSpeedMultiplier = 100f;

    private const float rayDistance = 10f;

    private NeuralNetwork network;

    private Rigidbody2D body;

    private bool crashed;

    public void Awake() {
        crashed = false;
        body = gameObject.GetComponent<Rigidbody2D>();
    }

    public void OnCollisionEnter2D(Collision2D collision) {
        crashed = true;
    }

    public void FixedUpdate() {
        if (!crashed) {

            double[] inputs = { rayDistance, rayDistance, rayDistance, rayDistance, rayDistance };

            RaycastHit2D hit;
            Vector3 hitDirection;

            // Left 90 Degrees
            hit = Physics2D.Raycast(transform.position, -transform.right, rayDistance, 1 << LayerMask.NameToLayer("Wall"));
            if (hit.collider != null) {
                hitDirection = new Vector3(hit.point.x - transform.position.x, hit.point.y - transform.position.y, 0);
                Debug.DrawRay(transform.position, hitDirection, Color.green);
                inputs[0] = hitDirection.magnitude;
            } else {
                Debug.DrawRay(transform.position, -transform.right * rayDistance, Color.green);
            }

            // Left 45 Degrees
            hit = Physics2D.Raycast(transform.position, transform.up - transform.right, rayDistance, 1 << LayerMask.NameToLayer("Wall"));
            if (hit.collider != null) {
                hitDirection = new Vector3(hit.point.x - transform.position.x, hit.point.y - transform.position.y, 0);
                Debug.DrawRay(transform.position, hitDirection, Color.yellow);
                inputs[1] = hitDirection.magnitude;
            } else {
                Debug.DrawRay(transform.position, (transform.up - transform.right).normalized * rayDistance, Color.yellow);
            }

            // Centre
            hit = Physics2D.Raycast(transform.position, transform.up, rayDistance, 1 << LayerMask.NameToLayer("Wall"));
            if (hit.collider != null) {
                hitDirection = new Vector3(hit.point.x - transform.position.x, hit.point.y - transform.position.y, 0);
                Debug.DrawRay(transform.position, hitDirection, Color.blue);
                inputs[2] = hitDirection.magnitude;
            } else {
                Debug.DrawRay(transform.position, transform.up * rayDistance, Color.blue);
            }

            // Left 45 Degrees
            hit = Physics2D.Raycast(transform.position, transform.up + transform.right, rayDistance, 1 << LayerMask.NameToLayer("Wall"));
            if (hit.collider != null) {
                hitDirection = new Vector3(hit.point.x - transform.position.x, hit.point.y - transform.position.y, 0);
                Debug.DrawRay(transform.position, hitDirection, Color.yellow);
                inputs[3] = hitDirection.magnitude;
            } else {
                Debug.DrawRay(transform.position, (transform.up + transform.right).normalized * rayDistance, Color.yellow);
            }

            // Right 90 Degrees
            hit = Physics2D.Raycast(transform.position, transform.right, rayDistance, 1 << LayerMask.NameToLayer("Wall"));
            if (hit.collider != null) {
                hitDirection = new Vector3(hit.point.x - transform.position.x, hit.point.y - transform.position.y, 0);
                Debug.DrawRay(transform.position, hitDirection, Color.green);
                inputs[4] = hitDirection.magnitude;
            } else {
                Debug.DrawRay(transform.position, transform.right * rayDistance, Color.green);
            }

            double[] outputs = network.GetOutput(inputs);

            double deltaPosition = outputs[0] * movementSpeedMultiplier;
            body.MovePosition(transform.position + transform.up * Time.fixedDeltaTime * (float) deltaPosition);

            double deltaBearing = outputs[1] * rotationSpeedMultiplier;
            body.MoveRotation(body.rotation + Time.fixedDeltaTime * (float) deltaBearing);
        }
    }

    public double GetFitness() {
        return transform.position.y;
    }

    public bool HasCrahsed() {
        return crashed;
    }

    public void SetNetwork(NeuralNetwork network) {
        this.network = network;
    }

    public NeuralNetwork GetNetwork() {
        return network;
    }
}
