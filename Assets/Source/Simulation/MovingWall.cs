﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingWall : MonoBehaviour {
	
	void FixedUpdate () {
		transform.position = new Vector2(transform.position.x, transform.position.y + Time.fixedDeltaTime);
	}

	public void ResetWall() {
		transform.position = new Vector2(0, -2);
	}
}
