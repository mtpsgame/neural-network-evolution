# Neural Network Evolution

This repository contains a Unity project that I have developed, which demonstrates how neural networks can be trained using genetic algorithms.

### The simulation

The individuals of the first generation are given random neural networks of a specified topology and activation function. The goal of the individuals is to travel as far as possible through a series of obstacles. If they collide with an obstacle then they cease to move. Once all the individuals of a generation have collided with an obstacle, the next generation is generated, by a genetic algorithm.

The genetic algorithm uses a tournament based selection to select two parents for each child. Roughly half the chromosomes (neural network weightings) of each parent combine to form the child. This process repeats until the number of children matches the initial population size.